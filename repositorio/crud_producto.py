from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from models.producto_modelo import Producto


def get_all(db: Session):
    productos = db.query(Producto).all()

    return productos

def get_by_id(id, db: Session):
    producto = db.query(Producto).get(id)

    if producto is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return producto


def create(producto, db: Session):
    producto = producto.dict()
    try:
        new_producto = Producto(
            nombre_producto=producto['nombre_producto'],
            descripcion = producto['descripcion'],
            precio=producto['precio']
        )
        db.add(new_producto)
        db.commit()
        db.refresh(new_producto)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,detail=f"error creando producto {e}")


def update(id, update_prod, db: Session):
    producto = get_by_id(id, db)

    producto.nombre_producto = update_prod.nombre_producto
    producto.descripcion = update_prod.descripcion
    producto.precio = update_prod.precio

    db.add(producto)
    db.commit()
    db.refresh(producto)

    return producto


def delete(id, db: Session):
    producto = get_by_id(id, db)

    db.delete(producto)
    db.commit()