
from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from models.producto_modelo import Categoria


def get_all(db: Session):
    categorias = db.query(Categoria).all()

    return categorias


def get_by_id(id, db: Session):
    categoria = db.query(Categoria).get(id)

    if categoria is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return categoria


def create(categoria, db: Session):
    categoria = categoria.dict()
    try:
        new_categoria = Categoria(
            categoria=categoria['categoria']
        )
        db.add(new_categoria)
        db.commit()
        db.refresh(new_categoria)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,detail=f"error creando producto {e}")


def update(id, update_cate, db:Session):
    categoria = get_by_id(id, db)

    categoria.categoria = update_cate.categoria

    db.add(categoria)
    db.commit()
    db.refresh(categoria)

    return categoria


def delete(id, db:Session):
    categoria = get_by_id(id, db)

    db.delete(categoria)
    db.commit()