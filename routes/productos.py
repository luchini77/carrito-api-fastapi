from fastapi import APIRouter, Path, status, Depends
from sqlalchemy.orm import Session

from schema.producto_schema import Producto
from repositorio import crud_producto
from config.config_bbdd import get_db


router_prod = APIRouter(prefix='/producto', tags=['Productos'])

#EL CRUD DE LOS PRODUCTOS

#LEER TODOS LOS PRODUCTOS
@router_prod.get('/', status_code=200)
def todos_los_productos(db: Session=Depends(get_db)):
    productos = crud_producto.get_all(db)
    return productos

#BUSCAR UN PRODUCTO
@router_prod.get('/{id}',status_code=200)
def un_producto(id:int=Path(ge=1), db: Session=Depends(get_db)):
    producto = crud_producto.get_by_id(id, db)
    return producto

#AGREGAR UN PRODUCTO
@router_prod.post('/', status_code=status.HTTP_201_CREATED)
def agregar_producto(producto: Producto, db: Session=Depends(get_db)):
    producto = crud_producto.create(producto, db)
    return {'respuesta':'producto creado exitosamente'}


#ACTUALIZAR UN PRODUCTO
@router_prod.put('/{id}', status_code=200)
def actualizar_producto(id:int, producto: Producto, db: Session=Depends(get_db)):
    res = crud_producto.update(id, producto, db)
    return res


#ELIMINAR UN PRODUCTO
@router_prod.delete('/{id}', status_code=200)
def eliminar_producto(id:int=Path(ge=1), db: Session=Depends(get_db)):
    res = crud_producto.delete(id, db)
    return res