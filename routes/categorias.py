from fastapi import APIRouter, Path, status, Depends
from sqlalchemy.orm import Session

from schema.producto_schema import Categoria
from repositorio import crud_categoria
from config.config_bbdd import get_db


router_cate = APIRouter(prefix='/categoria', tags=['Categorias'])


#EL CRUD DE LAS CATEGORIAS

#LEER TODAS LOAS CATEGORIAS
@router_cate.get('/', status_code=200)
def todos_las_categorias(db: Session=Depends(get_db)):
    categorias = crud_categoria.get_all(db)
    return categorias

#BUSCAR UNA CATEGORIA
@router_cate.get('/{id}',status_code=200)
def una_categoria(id:int=Path(ge=1), db: Session=Depends(get_db)):
    categoria = crud_categoria.get_by_id(id, db)
    return categoria

#AGREGAR UNA CATEGORIA
@router_cate.post('/', status_code=status.HTTP_201_CREATED)
def agregar_categoria(categoria: Categoria, db: Session=Depends(get_db)):
    categoria = crud_categoria.create(categoria, db)
    return {'respuesta':'categoria creada exitosamente'}


#ACTUALIZAR UNA CATEGORIA
@router_cate.put('/{id}', status_code=200)
def actualizar_categoria(id:int, categoria: Categoria, db: Session=Depends(get_db)):
    res = crud_categoria.update(id, categoria, db)
    return res


#ELIMINAR UNA CATEGORIA
@router_cate.delete('/{id}', status_code=200)
def eliminar_categoria(id:int=Path(ge=1), db: Session=Depends(get_db)):
    res = crud_categoria.delete(id, db)
    return res