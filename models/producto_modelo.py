from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey

from config.config_bbdd import base


class Producto(base):
    __tablename__='productos'
    id = Column(Integer, primary_key=True)
    nombre_producto = Column(String)
    descripcion = Column(Text)
    precio = Column(Integer)
    categoria_id = Column(Integer, ForeignKey('categorias.id', ondelete='CASCADE'))


class Categoria(base):
    __tablename__='categorias'
    id = Column(Integer, primary_key=True)
    categoria = Column(String)
    producto = relationship('Producto', backref='categorias',cascade="delete,merge")