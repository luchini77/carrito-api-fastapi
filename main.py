from fastapi import FastAPI
from fastapi.responses import HTMLResponse
import uvicorn

from routes.productos import router_prod
from routes.categorias import router_cate
from config.config_bbdd import base, motor


app = FastAPI()
app.title = 'Fuck Truck Kuka'


#BBDD
base.metadata.create_all(bind=motor)


#INICIO
@app.get('/', tags=['Inicio'])
def mensaje():
    return HTMLResponse('<h2>Api Carrito de la Kuka!</h2>')


app.include_router(router_prod)
app.include_router(router_cate)


if __name__=='__main__':
    uvicorn.run('main:app', port=8000, reload=True)