from pydantic import BaseModel
from typing import Optional


class Producto(BaseModel):
    id: Optional[int]=None
    nombre_producto: str
    descripcion: str
    precio: int

    class Config:
        from_attributes=True
        json_schema_extra = {
            'example':{
                'nombre_producto':'solo',
                'descripcion':'palta, mayonesa casera',
                'precio':5000
            }
        }

class Categoria(BaseModel):
    id: Optional[int]=None
    categoria: str

    class Config:
        from_attributes=True
        json_schema_extra= {
            'example':{
                'categoria':'completo'
            }
        }